"""
Author: Satyam Dixit

THis program tells you the repeating characters in the list

"""

from collections import defaultdict

def firstRepeat(randomList):
    d= defaultdict(int)
    for i in randomList:
        if d[i]>0:
            print("First repeating character is "+i)
            break
        else:
            d[i]+=1

def allRepeatingCharacter(randomList):
    d= defaultdict(int)
    repeating=[]
    for i in randomList:
        if d[i]>0:
            repeating.append(i)
        else:
            d[i]+=1
    print("Repeating characters are ")
    print(repeating)

firstRepeat('SatyamDixit')

allRepeatingCharacter('SatyamDixit')