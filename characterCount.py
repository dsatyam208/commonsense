"""
This program counts the number of characters in a given string
"""

def characterCount(str):
    d = {}
    for i in str:
        if(i in d.keys()):
            d[i]+=1
        else:
            d[i]=1
    print("Character count for the given string is as follows:")
    for k,v in d.items():
        print(k,v)

characterCount('pythonprogramming')