"""
Autohr: Satyam Dixit

This program tells you the count of even numbers for a given range of numbers
"""

def countEven():
    countEven=0
    countOdd=0
    for i in range(50,119):
        if(i%2==0):
            countEven+=1
        else:
            countOdd+=1

    print("Total count of even number is "+str(countEven)+" and odd is "+str(countOdd))

countEven()