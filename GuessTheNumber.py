"""
This program is to guess a number between given 2 numbers, maximum allowed guesses are 6
"""
import random

def guessTheNumber(numberTobeGuessed, numberOfGuesses, nameOfPlayer):
    for i in range(1,numberOfGuesses):
        try:
            print("Take a guess!")
            playerGuess=int(input())
            if(playerGuess>numberTobeGuessed):
                print("Your guess is higher than the number")
            elif(playerGuess<numberTobeGuessed):
                print("Your guess is lower than the number")
            else:
                break
        except:
            print("Sorry, you entered an invalid format for the number, try agian")
    if(playerGuess==numberTobeGuessed):
        print("Good job! " + nameOfPlayer + " You have guessed the number in " + str(i) + " attempt")
    else:
        print("Allowed limit for guesses is over\n"
              "Hard Luck! Better luck next time")
print("Hi!, Welcome to guess a number game\n"
      "Please provide your name")
nameOfPlayer = input()
print(nameOfPlayer+" the number lies between 1 and 20, please make your guess")
numberTobeGuessed=random.randint(1,20)
guessTheNumber(numberTobeGuessed,6,nameOfPlayer)